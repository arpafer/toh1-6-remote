// Copyright (c) 2016, arpafer. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import "dart:async";
import 'package:angular2/core.dart';
import "package:angular2/router.dart";
import "Hero.dart";
import 'hero_detail_component.dart';
import "hero_service.dart";

@Component(
    selector: 'my-heroes',
    styleUrls: const ['heroes_component.css'],
    templateUrl: 'heroes_component.html',

    directives: const [HeroDetailComponent]
    
)

class HeroesComponent implements OnInit {
	
	//Hero hero = new Hero(1, "Mr Elastico");

    Hero selectedHero;	

    final HeroService _heroService;
    final Router _router;

    HeroesComponent(this._heroService, this._router);

	List<Hero> heroes = null;

    onSelect(Hero hero) {

    	selectedHero = hero;
    }	

    Future<Null> getHeroes() async {

    	heroes = await _heroService.getHeroes();
    }

    void ngOnInit() {

    	getHeroes();
    }

    Future<Null> gotoDetail() => _router.navigate([
        'HeroDetail',
        {'id': selectedHero.id.toString()}
      ]);
}




