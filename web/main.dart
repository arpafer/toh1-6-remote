// Copyright (c) 2016, arpafer. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/platform/browser.dart';

import 'package:toh1_6/app_component.dart';

main() {
  bootstrap(AppComponent);
}
